package com.example.databindingmodelview.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.databindingmodelview.model.User

class ViewModel : ViewModel() {
    var user : MutableLiveData<User> = MutableLiveData()
    var visibility : MutableLiveData<Boolean> = MutableLiveData()

    init {
        visibility.value = true
    }


    fun updateUser(){
        this.user.value = User("Thibaut", "26")
    }


    fun updateVisibility(){
        this.visibility.value = !this.visibility.value!!
    }
}