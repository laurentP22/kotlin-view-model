package com.example.databindingmodelview.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.databindingmodelview.R
import com.example.databindingmodelview.databinding.SecondActivityBinding
import com.example.databindingmodelview.model.User
import com.example.databindingmodelview.viewmodel.ViewModel

class SecondActivity : AppCompatActivity() {
    private lateinit var viewModel : ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : SecondActivityBinding = DataBindingUtil.setContentView(this, R.layout.second_activity)
        binding.lifecycleOwner = this

        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.user.value = User("Alberto", "30")

    }
}