package com.example.databindingmodelview.view

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.databindingmodelview.R


class MainActivity : AppCompatActivity() {
    private lateinit var btnSecondActivity:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSecondActivity = findViewById(R.id.btnSecondActivity)
        btnSecondActivity.setOnClickListener{
            startActivity(Intent(applicationContext, SecondActivity::class.java))
        }
    }
}
